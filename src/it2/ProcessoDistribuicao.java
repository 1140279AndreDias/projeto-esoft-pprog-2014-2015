package it2;

public interface ProcessoDistribuicao extends Distribuicao {

	/**
	 * 
	 * @param m
	 */
	void setMecanismo(int m);

	/**
	 * 
	 * @param es
	 */
	void distribuir(int es);

	void getInfo();

	void novaRevisao();

}